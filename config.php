<?php

return [
    'baseUrl' => '',
    'production' => false,
    'site_name' => 'New Horizons Dental Laboratory',
    'collections' => [
        'products' => [
            'path' => '/products/'
        ],
        'full-arch' => [
            'path' => '/products/full-arch-restorations/'
        ],
        'removable' => [
            'path' => '/products/removable/'
        ],
        'resources' => [
            'path' => '/resources/'
        ],
        'implants' => [
            'path' => '/products/implants/'
        ],
        'services' => [
            'path' => '/products/services/'
        ],
        'sendcase' => [
            'path' => '/send-case/'
        ],
        'about-us' => [
            'path' => '/about-us/'
        ]
    ]
];
