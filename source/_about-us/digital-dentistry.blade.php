@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Digital Dentistry',
    'meta_description' => 'New Horizons Dental Laboratory is eagerly integrating digital dentistry into our workflow that will benefit our lab and our clients\' practices.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Digital Dentistry'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Ensuring Quality Through the Latest Innovations</h2>
                <p>New Horizons Dental Laboratory believes that digital dentistry improves the quality, comfort, and speed of dental restorations. Digital innovations provide ongoing benefits to our team and your practice by streamlining our processes and ensuring superior collaborations. As we strive to always provide exceptional restorations and services to our clients, we believe our digital growth is never finished.</p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h3>Benefits of Digital Dentistry</h3>
            <p>We accept digital scans from most major intraoral scanners which allow us to provide increased benefits to your patients and your practice.</p>
            <p>• Greater speed •<br>
            • Improved precision •<br>
            • Greater aesthetics and comfort •<br>
            • Improved patient satisfaction •</p>
        </div>
    </div>
</section>
<hr>
<section class="container">
    <div class="row">
      <div class="col-12 text-center">
          <h3 class="mb-4">Our Technology</h3>
      </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <img src="/img/KaVo-LS-3-Scanner.png" alt="KaVo LS 3 Scanner"> 
        </div>
        <div class="col-sm-12 col-md-8 align-self-center">
            <h4>KaVo LS 3 Scanner</h4>
            <p>The KaVo LS 3 full articulator scanner allows the NHDL team to accelerate our day-to-day work. It is built to provide speed and accuracy, so we can ensure high precision for every case. The KaVo LS 3 is also integrated with NobelProcera, so we can provide the best implant-based removables possible.</p>
            <ul>
                <li>Full Articulator Scanning</li>
                <li>Proven Accuracy</li>
                <li>Quality Scans with Speed</li>
                <li>Full-Color Scanning</li>
                <li>Full Connectivity</li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-4">
            <img src="/img/DTX-Studio-Design-Software.png" alt=""> 
        </div>
        <div class="col-sm-12 col-md-8 align-self-center">
            <h4>DTX Studio™ Design Software</h4>
            <p>We have equipped the KaVo LS 3 scanner with the DTX Studio™ design software from Nobel Biocare®, which allows us to easily collaborate and share data with clinicians.</p>
            <ul>
                <li>Ensures Up to Date Case Info</li>
                <li>Precise Restorations</li>
                <li>Intuitive CAD Workflows</li>
                <li>Streamlines Complex Cases</li>
                <li>Implant Bar Design</li>
            </ul>
        </div>
    </div>
    </section>
@endsection

@section('scripts')

@endsection