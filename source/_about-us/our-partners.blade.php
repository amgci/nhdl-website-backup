@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Our Partners',
    'meta_description' => 'New Horizons Dental Laboratory is proud to be partnered with dental industry leaders who assist us in providing the best restorations and service possible. '
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Our Partners'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>New Horizons Dental Laboratory is proud to be partnered with dental industry leaders who assist us in providing the best restorations and service possible. </p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h3>Manufacturer Partners</h3>
            <p>Our lab works with all implant systems.</p>
        </div>
    </div>
    <div class="row">
        <div class="offset-md-1 col-sm-2">
            <img src="/img/Nobel-Biocare-Logo.png" alt="Nobel Biocare™ ">
        </div>
        <div class="col-sm-2">
            <img src="/img/Straumann_Logo.svg.png" alt="Straumann®">
        </div>
        <div class="col-sm-2">
            <img src="/img/biomet-3i-logo-blue.png" alt="3i">
        </div>
        <div class="col-sm-2">
            <img src="/img/Thommen_Medical_logo.png" alt="Thommen">
        </div>
        <div class="col-sm-2">
            <img src="/img/titan_logo.png" alt="Titan">
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-12 text-center">
            <h3>Nonprofit Organizations We Support</h3>
            <img src="/img/Proud-Supporter-Digital-Badge.png" style="max-width: 200px;margin-top: 2rem;" alt="Dental Lifeline Network">
        </div>
    </div>
</section>
@endsection

@section('scripts')

@endsection






