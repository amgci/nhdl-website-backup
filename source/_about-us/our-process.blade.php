@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Our Process',
    'meta_description' => 'As your partner, it is our responsibility to deliver functional and attractive cases on schedule that fit the first time.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Our Process'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Ensuring Every Case Ends with Success</h2>
                <p>As your partner, it is our goal to deliver functional and attractive cases on schedule that fit the first time. Our unique process and resources are committed to eliminating problems before they occur.</p>

                <p>Together we must achieve clarity on how our process works to ensure that you are prepared to deliver efficient and effective results to your patients. At New Horizons Dental Laboratory, we are in constant pursuit of patient satisfaction. Take a look at an example of one of our processes below.</p>
                
                <p><a href="/resources/forms/" class="btn">View Our Process</a></p>
            </div>
        </div>
</section>
@endsection

@section('scripts')

@endsection