@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Our Story',
    'meta_description' => 'New Horizons Dental Laboratory stands shoulder-to-shoulder with dentists in pursuit of a common goal: dental health and patient satisfaction.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Our Story'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>The NHDL Vision</h2>
                <h4 class="font-italic">New Horizons Dental Laboratory stands shoulder-to-shoulder with dentists in pursuit of a common goal: dental health and patient satisfaction.</h4>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12">
            <p>New Horizons Dental Laboratory stands shoulder-to-shoulder with dentists in pursuit of a common goal: dental health and patient satisfaction.</p>

            <p>New Horizons Dental Laboratory has been operating in Broomfield, CO for over 30 years. Since our founding, we have been the laboratory of choice for clinicians in search of expertly crafted, high-quality removable and fixed-removable restorations. Over these 30 years we have been able to serve clinicians throughout Colorado.</p>

            <p>In September of 2016, Champa Patel, the new owner came on board full-time and began working alongside the previous owner. Champa reinvigorated our laboratory team with her strong work ethic and belief that NHDL's purpose was more than simply providing dental appliances. We are comprised of a dedicated team that prioritizes the happiness of patients through our focus on providing comprehensive support that meets a clinician's every need.</p> 

            <p>In 2018, Champa's passion and expertise was evident when she won the Nobel Procera Bar Design Competition at the mid-winter LMT Chicago Conference. This tremendous accomplishment fueled her desire to bring innovation to the dental lab industry by aligning dentists and NHDL under the north star of improved patient satisfaction. The success of our laboratory is due to our understanding that ultimately our laboratory and clinicians have the same goal in mind: improving quality of life for patients through quality dentistry.</p> 

            <p>Today, the New Horizons Dental Laboratory team is guided by Champa Patel's vision. We are not merely a dental lab; New Horizons Dental Laboratory represents an exclusive methodology that goes beyond creating dental appliances. As our expert technicians work hard every day in our state-of-the-art laboratory, the true product they are crafting is patient satisfaction.</p> 

            <p>"NHDL gives me the courage and knowledge to begin bigger cases." - Dr. Harald Joesaar</p>
        </div>
    </div>
</section>
@endsection

@section('scripts')

@endsection