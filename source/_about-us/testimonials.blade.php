@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Testimonials',
    'meta_description' => 'Discover why NHDL is the go-to laboratory for dentists by reading testimonials from our clients.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Testimonials'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Thank You to All Our Dental Partners for Sharing Their Success Stories</h2>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12">
            <p>"I have been working with New Horizons for a decade and entrust them with my most difficult cases. The level of precision and craftsmanship they provide has resulted in countless satisfied patients and makes even the most challenging cases predictable."</p>
            <p class="font-weight-bold">- Andrew J. Bock, DDS, FACP</p>
            <hr>
        </div>
        <div class="col-12">
            <p>"First, let me say New Horizons Dental Lab is one of the finest removable prosthetic labs in Colorado. I've used them for about 25 years. I am semi-retired now, and when I need to help a friend or prior patient with a removable case, I contact them. I just finished an implant retained removable bar case. At delivery, no problems. If you have any questions, or need a little tutoring, they have all the answers."</p>
            <p class="font-weight-bold">- Walter Davis, DDS</p>
            <hr>
        </div>
        <div class="col-12">
            <p>"NHDL gives me the courage and knowledge to begin bigger cases."</p>
            <p class="font-weight-bold">- Dr. Harald Joesaar</p>
            <hr>
        </div>
        <div class="col-12">
            <p>"NHDL is good at answering technical questions."</p>
            <p class="font-weight-bold">- Dr. Chris Chamberlain</p>
            <hr>
        </div>
        <div class="col-12">
            <p>"I can generally expect the highest quality standards from NHDL, which solves the problems that come with lower quality."</p>
            <p class="font-weight-bold">- Dr. Derek Petersen</p>
            <hr>
        </div>
        <div class="col-12">
            <p>"Complex cases are solved - they give me reliable results, I don't have to re-do work, they get it right the first time. Speed is good, patients aren't forced to wait a long time."</p>
            <p class="font-weight-bold">- Dr. Andrew Bock</p>
            <hr>
        </div>
        <div class="col-12">
            <p>"Their dentures are always the best fitting and looking. The night guards are great too. Minimal adjustments needed."</p>
            <p class="font-weight-bold">- Dr. Larry Tilliss</p>
            <hr>
        </div>
        <div class="col-12">
            <p>"NHDL's Accuracy and knowledge create well-designed and well-fitting appliances"</p>
            <p class="font-weight-bold">- Dr. Kari Amick</p>
            <hr>
        </div>
    </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12 text-center">
            <h3>Do You Have a Testimonial You Would Like to Share? </h3>
            <form id="testimonial-form" action="">
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="name-contactform" class="form-control" placeholder="Your Name" required="required" type="text" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="practice-name-contactform" class="form-control" placeholder="Practice Name" required="required" type="text" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="phone-contactform" class="form-control" placeholder="Phone Number" required="required" type="tel" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <textarea id="message-contactform" class="form-control" placeholder="Your Testimonial"></textarea>
                </div>
                <input type="hidden" id="public_id" value="71eaa30aa80a4879ab0da2a6dac07fa9" />
                <div class="g-recaptcha" data-sitekey="6LcFe5gUAAAAAC2ridE-9fv03DtO5uC3M80nWE80"></div>
                <button class="btn btn-primary mt-3" type="submit">Send Now</button>
            </form>
            <div class="loader">Loading...</div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#testimonial-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/71eaa30aa80a4879ab0da2a6dac07fa9/3e2bd345ebd64d6898161d38ea982f34',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-contactform').eq(0).val(),
                    name: $('#name-contactform').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#testimonial-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#testimonial-form').show();
                    formPending = false;
                    $('#testimonial-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection