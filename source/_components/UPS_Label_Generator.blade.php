<div class="form-container">
    <form id="shipping-label-form" action="">
        <div class="form-label-group mb-3" style="margin: auto;">
            <input class="form-control" id="doctor-name-labelform" placeholder="Doctor's Name" required="required" type="text" />
        </div>
        <div class="form-label-group mb-3" style="margin: auto;">
            <input class="form-control" id="practice-name-labelform" placeholder="Practice Name" required="required" type="text" />
        </div>
        <div class="form-label-group mb-3" style="margin: auto;">
            <input class="form-control" id="phone-labelform" placeholder="Phone Number" required="required" type="text" />
        </div>
        <div class="form-label-group mb-3" style="margin: auto;">
            <input class="form-control" id="email-labelform" placeholder="Email Address" required="required" type="text" />
        </div>
        <div class="form-label-group mb-3" style="margin: auto;">
            <input class="form-control" id="address-labelform" placeholder="Address" required="required" type="text" />
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input class="form-control" id="city-labelform" placeholder="City" required="required" type="text" />
                </div>
            </div>
            <div class="col-lg-2 px-lg-0">
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input class="form-control" id="state-labelform" placeholder="State" required="required" type="text" maxlength="2" />
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input class="form-control" id="zip-labelform" placeholder="Zipcode" required="required" type="text" maxlength="5" />
                </div>
            </div>
        </div>
        <input type="hidden" name="service" value="03">
        <input type="hidden" id="public_id" value="79e174c728bd467b9ecdf2c5f34294f5" />
        <button type="submit" class="btn btn-primary">Create Label</button>
    </form>
    <div class="loader">Loading...</div>
</div>