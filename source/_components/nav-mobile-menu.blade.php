<div id="closeIcon">
    <span>X</span>
</div>
<ul class="list-unstyled d-lg-flex">
    <li><a href="/#"><span class="h-effect"></span><span class="nav-link-txt">Home</span></a></li>
    <li>
        <a href=""  data-target="#about-collapse-mobile" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">About Us</span><div class="sub-tri"></div></a>
        <ul class="collapse sub-menu" id="about-collapse-mobile" data-parent="#primary-nav">
            <li><a href="/about-us/our-story">Our Story</a></li>
            <li><a href="/about-us/our-process">Our Process</a></li>
            <li><a href="/about-us/digital-dentistry">Digital Dentistry</a></li>
            <li><a href="/about-us/testimonials">Testimonials</a></li>
            <li><a href="/about-us/our-partners">Our Partners</a></li>
        </ul> 
    </li>
    <li>
        <a href=""  data-target="#products-collapse-mobile" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Products</span><div class="sub-tri"></div></a>
        <ul class="collapse sub-menu" id="products-collapse-mobile" data-parent="#primary-nav">
            <li><a href="/products/full-arch-restorations">Full Arch Restorations</a></li>
            <li><a href="/products/removables">Conventional Removables</a></li>
            <li><a href="/products/night-guards">Night Guards</a></li>
            <li><a href="/products/additional-services">Additional Services</a></li>
        </ul> 
    </li>
    <li>
        <a href=""  data-target="#send-case-collapse-mobile" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Send a Case</span><div class="sub-tri"></div></a>
        <ul class="collapse sub-menu" id="send-case-collapse-mobile" data-parent="#primary-nav">
            <li><a href="/send-case/new-doctor">New Dentist</a></li>
            <li><a href="/resources/forms/">Rx Forms</a></li>
            <li><a href="/send-case/local-pickup">Local Pick-Up</a></li>
            <li><a href="/send-case/case-scheduler">Case Scheduler</a></li>
            <li><a href="/send-case/turn-around-times">Turnaround Times</a></li>
            <li><a href="/send-case/request-supplies">Request Supplies</a></li>
        </ul>
    </li>
    <li>
        <a href=""  data-target="#resources-collapse-mobile" aria-expanded="false" data-toggle="collapse"><span class="h-effect"></span><span class="nav-link-txt">Resources</span><div class="sub-tri"></div></a>
        <ul class="collapse sub-menu" id="resources-collapse-mobile" data-parent="#primary-nav">
            <li><a href="/resources/forms">Forms</a></li>
            <li><a href="/resources/case-studies">Case Studies</a></li>
        </ul>
    </li>
    <li><a href="/contact-us"><span class="h-effect"></span><span class="nav-link-txt">Contact Us</span></a></li>
</ul>