@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'All-on-4®',
    'meta_description' => 'New Horizons Dental Lab is proud to offer the innovative all-on-4® restorative treatment that utilizes as little as four implant to support a full-arch prosthesis.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Full Arch Restorations'])
<section class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/Zirconia-Hybrid-Bridge.png" alt="All-on-4®">
            </div>
            <div class="col-md-8">
                <h2>All-on-4®</h2>
                <p>New Horizons Dental Laboratory is proud to offer the innovative All-on-4® restorative treatment that utilizes as little as four implant to support a full-arch prosthesis. This option is an ideal solution for many patients due to its maximization of the available bone in the edentulous ridge. Due to the frequent receding of bone that many patients face with tooth loss, the All-on-4® treatment allows them to gain the benefits of a fixed prosthesis without the need for grafting. The All-on-4® treatment frequently uses two straight, anterior abutments and two angulated posterior abutments (with the angulation up to 45°), which ensures a stable placement in the patient's bone and the eventual osseointegration of the implants. All-on-4® also allows for the immediate loading of implants, which is a tremendous benefit to the patient. </p>
                <h3>Features</h3>
                <ul>
                    <li>All-on-4® includes: screw retained fixed, attached bar overdentures, and direct attached overdentures.</li>
                    <li>Trefoil™</li>
                    <li>Traditional Dentures</li>
                </ul>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection