@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Removable Implant Retained Restoration',
    'meta_description' => 'New Horizons Dental Lab is proud to offer the innovative all-on-4® restorative treatment that utilizes as little as four implant to support a full-arch prosthesis.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Full Arch Restorations'])
<section class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/DentureWithOverbar-Allon4.png" alt="Removable Implant Retained Restoration">
            </div>
            <div class="col-md-8">
                <h2>Removable Implant Retained Restoration</h2>
                <p>For your removable needs in addition to traditional dentures, NHDL offers the following options for implant retained restorations: Direct Attached Overdentures with Locators and/or Ball Attachments.</p>
                <h3>We Offer the Following Options for a Removable Implant Retained Restoration:</h3>
                <p>For your removable needs in addition to traditional dentures, NHDL offers the following options for implant retained restorations: Direct Attached Overdentures with Locators and/or Ball Attachments.</p>
                <h4>Attached Bar Overdentures</h4>
                <p>The overdenture provides patients with stability and lifelike function through the use of a titanium bar secured locators that attach to the implant-retained bar. The overdenture is easily removable by the patient or doctor for cleaning or repairs. The overdenture is a popular solution due to its duality as a fixed removable solution that provides functionality and retrievability. It is available with the Dolder, Hader, or Paris bars.</p>
                <hr>
                <h4>Direct Attached Overdentures</h4>
                <p>The overdenture provides patients with stability and lifelike function through the use of secure locators that attach the denture to the implants themselves without the need for a titanium bar. The overdenture is easily removable by the patient or doctor for cleaning or repairs. The overdenture is a popular solution due to its duality as a fixed removable solution that provides functionality and retrievability as well as strength by the use of a Meso Structure within the denture.</p>
                <hr>
                <h4>Repair/Complete Retread</h4>
                <p>NHDL offers repair services for All-on-4® restorations. If your patient's fixed prosthetic is showing signs of wear from years of use. NHDL will completely remove all old acrylic and teeth and retread the prosthesis with a new set of teeth and new acrylic over the existing titanium bar, in essence giving your patient a whole new prosthesis. </p>
                <p>If you need a repair "retread", please <a href="/contact-us/">contact</a> us today.</p>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection