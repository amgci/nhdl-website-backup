<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        @yield('meta')
        <meta name="robots" content="noydir" />
        <meta http-equiv="Cache-Control" CONTENT="no-cache">
        <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
        <link rel="icon" type="image/png" href="/img/8249-Favicon.png">
        <link rel="stylesheet" href="/assets/css/main.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Quattrocento+Sans:400,700" rel="stylesheet"> 
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
    </head>
    <body class="p-{{ $page->getFilename() }}">
        <header>
            <div class="container">
                <div class="row">
                    <div class="d-flex col-12" id="main-header">
                        <div>
                            <a href="/">
                                <img id="header-logo" src="/img/NHDL-Logo-RGB_Logo.svg" alt="New Horizons Dental Laboratory">
                            </a>
                        </div>
                        <div>
                            <div class="main-nav-wrap">
                                @include('_components.nav-menu')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="mobile-menu" class="d-block d-lg-none">
            @include('_components.nav-mobile-menu')
        </div>
        <main role="main">
            @yield('body')
        </main>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h5>New Horizons Dental Laboratory</h5>
                        <address>7270 W. 118th Place UNIT D • Broomfield, Colorado 80020 <br>
                        Phone: 303.469.3362 • Fax: 303.469.0002</address>
                    </div>
                    <div class="col-6 offset-sm-3 text-center">
                        <!-- SharpSpring Form for NHDL - Specials Sign-Up  -->
                        <span style="font-weight: bold;font-size: 18px;margin-bottom: -20px;">Sign Up for Specials and Promotions</span>
                        <script type="text/javascript">
                            var ss_form = {'account': 'MzawMDE3NzY1AAA', 'formID': 'SzFPszRNsjTQNTVLTtQ1MUpJ1E00SLTQtTA2TTFLTTYyME6yAAA'};
                            ss_form.width = '100%';
                            ss_form.height = '1000';
                            ss_form.domain = 'app-3QNJWPZVN4.marketingautomation.services';
                            // ss_form.hidden = {'field_id': 'value'}; // Modify this for sending hidden variables, or overriding values
                            // ss_form.target_id = 'target'; // Optional parameter: forms will be placed inside the element with the specified id
                            // ss_form.polling = true; // Optional parameter: set to true ONLY if your page loads dynamically and the id needs to be polled continually.
                        </script>
                        <script type="text/javascript" src="https://koi-3QNJWPZVN4.marketingautomation.services/client/form.js?ver=2.0.1"></script>
                    </div>
                    <div class="col-12 d-flex justify-content-center">
                        <div>
                            <a href="https://www.facebook.com/NewHorizonsDentalLab/" target="_blank">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 72 72" style="enable-background:new 0 0 72 72;" xml:space="preserve">
                                <path class="st0" d="M44.5,23.3h4.2v-8.5h-8.2c-8.2,0-6.6,8.5-6.6,8.5v6.4h-4.2v8.5h4.2v19.1h8.5V38.1h4.2l2.1-8.5h-6.4v-4.2
                                    C42.4,23.7,44.5,23.3,44.5,23.3z M36,69.9C17.3,69.9,2.1,54.7,2.1,36S17.3,2.1,36,2.1S69.9,17.3,69.9,36S54.7,69.9,36,69.9z"/>
                                </svg>
                            </a>
                        </div>
                        <div>
                            <a href="https://www.linkedin.com/company/new-horizons-dental-laboratory-broomfield-colorado/" target="_blank">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 72 72" style="enable-background:new 0 0 72 72;" xml:space="preserve">
                                <path class="st0" d="M55.1,34.8c0-3-1-5.3-2.9-6.8c-2.2-1.8-4.3-2.7-7.4-2.7c-1.5,0-2.5,0.2-3.9,0.7c-1.5,0.5-3,1.5-4.8,2.8v-3.4
                                    h-6.9v25.4H36v-9.2c0-2.7,0.1-4.5,0.3-5.3c0.4-1.6,1.2-2.8,2.4-3.7c1.1-0.9,2.5-1.3,4.1-1.3c1.2,0,2.2,0.3,3.1,0.8
                                    c0.8,0.5,1.4,1.3,1.8,2.3c0.3,1,0.5,3.1,0.5,6.2v10.2h6.9V34.8z M23.9,25.4h-6.9v25.4h6.9V25.4z M22.9,17.9c-0.7-0.7-1.5-1-2.5-1
                                    c-0.9,0-1.8,0.3-2.4,1c-0.7,0.7-1,1.4-1,2.3c0,1,0.3,1.8,1,2.4c0.7,0.7,1.5,1,2.5,1c1,0,1.8-0.3,2.4-1c0.7-0.7,1-1.5,1-2.4
                                    S23.5,18.6,22.9,17.9z M36,69.9C17.3,69.9,2.1,54.7,2.1,36S17.3,2.1,36,2.1S69.9,17.3,69.9,36S54.7,69.9,36,69.9z"/>
                                </svg>
                            </a>
                        </div>
                        <div>
                            <a href="/contact-us">
                                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                     viewBox="0 0 72 72" style="enable-background:new 0 0 72 72;" xml:space="preserve">
                                <path class="st0" d="M55.1,26.1l-10.4,8.1l5.8,9.2L43,35.5L36,41l0,0l0,0l-7-5.5l-7.5,7.9l5.8-9.2l-10.4-8.1v22.6H36h19.1V26.1z
                                     M52.8,23.3H36H19.2c-0.7,0-1.3,0.3-1.7,0.8L36,38.6l0,0l0,0l18.4-14.5C54.1,23.6,53.5,23.3,52.8,23.3z M36,69.9
                                    C17.3,69.9,2.1,54.7,2.1,36S17.3,2.1,36,2.1S69.9,17.3,69.9,36S54.7,69.9,36,69.9z"/>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div id="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 d-flex" style="align-self: center;"><span>Copyright &copy; {{ date("Y") }} - <em>All Rights Reserved</em></span></div>
                    <div class="col-12 col-md-6" style="text-align: right;"><a href="https://amgci.com/?cref=1" target="_blank"><img src="/img/DesignedBy-AMGLogo.svg" alt="Website Design by AMG Creative, Inc." width="192" height="45" /></a></div>
                </div>
            </div>
        </div>
        
        <script src="/assets/js/script.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        @yield('scripts')
        
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137738297-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-137738297-1');
        </script>
   
        <!-- Browser Compatibility -->
        <script> 
            var $buoop = {required:{e:-0.01,i:999,f:-1,o:0,s:0,c:-1},insecure:true,unsupported:true,api:2018.09,reminder:0,reminderClosed:1,no_permanent_hide:true }; 
            function $buo_f(){ 
             var e = document.createElement("script"); 
             e.src = "//browser-update.org/update.min.js"; 
             document.body.appendChild(e);
            };
            try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
            catch(e){window.attachEvent("onload", $buo_f)}
        </script>
        
        <!-- SharpSpring Tracking Beacon -->
        <script type="text/javascript">
            var _ss = _ss || [];
            _ss.push(['_setDomain', 'https://koi-3QNJWPZVN4.marketingautomation.services/net']);
            _ss.push(['_setAccount', 'KOI-458YWB16LE']);
            _ss.push(['_trackPageView']);
        (function() {
            var ss = document.createElement('script');
            ss.type = 'text/javascript'; ss.async = true;
            ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNJWPZVN4.marketingautomation.services/client/ss.js?ver=2.2.1';
            var scr = document.getElementsByTagName('script')[0];
            scr.parentNode.insertBefore(ss, scr);
        })();
        </script>
    </body>
</html>
