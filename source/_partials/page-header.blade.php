<div id="page-header">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>{{ $page_title }}</h1>
            </div>
        </div>
    </div>
</div>