@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Additional Services',
    'meta_description' => 'To increase efficiency, patient satisfaction, and overall success, the New Horizons Dental Laboratory team is happy to provide value-added services.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Additional Services'])
<section class="container">
        <div class="row">
           <div class="col-12 text-center">
                <p>As your dental laboratory partner, it is our goal to ensure every aspects of your cases are stress-free and streamlined. To increase efficiency, patient satisfaction, and overall success, the New Horizons Dental Laboratory team is happy to provide value-added services. If you are interested in any service, please contact our team today to learn more or schedule an appointment. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/Avadent_Denture.png" alt="Reline and Repair">
            </div>
            <div class="col-md-8 align-self-center">
                <h3>Reline &amp; Repair</h3>
                <p>Even a prosthetic with superior fabrication cannot be expected to last a lifetime without adjustments or repairs. The New Horizons Dental Laboratory team will gladly reline or repair your patient's prosthetic to ensure renewed comfort and function. Please contact our lab to discuss the details and timelines of your needed repair.</p>
                <p>NHDL also offers <a href="/products/full-arch-restorations/implant-restorations/">repairs and retreads</a> for implant-retained Full-Arch Restorations.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/Design-Services.png" alt="Scanning and Diagnostic Services">
            </div>
            <div class="col-md-8 align-self-center">
                <h3>Scanning and Diagnostic Services</h3>
                <p>New Horizons Dental Laboratory is proud to be an advocate and integrator of digital dentistry into the laboratory. We want to put our own technology to work for your practice, which is why we offer scanning and diagnostic services. We will gladly take your patient's impression or model and provide a full diagnostic scan for the case.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/Chairside-Conversion-2.png" alt="Chair-side Assistance">
            </div>
            <div class="col-md-8 align-self-center">
                <h3>Chair-side Assistance</h3>
                <p>The experts at New Horizons Dental Laboratory, are available for a small fee, to provide chair-side assistance during surgery in the Denver metro area. We can convert your patient's traditional denture into an immediately loaded provisional and deliver the next day which will help protect the healing abutments until the final restoration can be placed after oseointegration. Having a member of our team at your side can easily turn a complex case into a stress-free process. In addition, your patient will be highly satisfied to have the prosthetic in their mouth for the duration of the healing process. </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/Printed-Model.png" alt="Model Pouring Classes">
            </div>
            <div class="col-md-8 align-self-center">
                <h3>Model Pouring Classes</h3>
                <p>At New Horizons Dental Laboratory, we understand how vital the pouring of an accurate dental impression is to ensuring a successful case outcome. We provide model pouring classes to help your staff in mastering this skill, which can increase efficiency and accuracy. Please <a href="/contact-us/">contact us</a> today about our next available class. </p>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection