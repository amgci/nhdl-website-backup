@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full Arch Restorations',
    'meta_description' => 'NHDL offer expertly crafted full arch restorations which will allow your patients to continue their daily lives with a stable and long lasting smile.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Full Arch Restorations'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>At New Horizons Dental Laboratory, we understand that often your edentulous patients would rather not have to deal with a denture. Our focus is always on patient satisfaction, therefore we provide several options for full arch solutions. </p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row product-listing">
        <div class="col-md-4">
            <div class="product-thumb">
                <a href="/products/full-arch-restorations/all-on-4">
                    <img src="/img/Zirconia-Hybrid-Bridge.png" alt="All-on-4®">
                    <h4>All-on-4®</h4>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="product-thumb">
                <a href="/products/full-arch-restorations/implant-restorations">
                    <img class="product-thumb" src="/img/DentureWithOverbar-Allon4.png" alt="Removable Implant Retained Restoration">
                    <h4>Removable Implant Retained Restoration</h4>
                </a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection