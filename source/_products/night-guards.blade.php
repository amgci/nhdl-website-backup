@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Night Guards',
    'meta_description' => 'New Horizons Dental Laboratory offers two different options for night guards depending on your patient\'s needs and severity of their bruxism.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Night Guards (Bruxism Splints)'])
<section class="container">
        <div class="row">
           <div class="col-12 text-center">
                <p>Night guards are an excellent option for patients who experience nighttime bruxism. We fabricate our night guards out of high-quality acrylic to ensure it will fit perfectly in your patient's mouth and not interfere with their sleep. We offer two different options for night guards depending on your patient's needs and severity of their bruxism. </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/comfort-hs-NightGuard.png" alt="Hard Night Guard">
            </div>
            <div class="col-md-8 align-self-center">
                <h3>Hard Night Guard</h3>
                <p>This night guard is fabricated out of a hard, heat-cured acrylic that can withstand even severe nighttime bruxism. </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/NightGuard.png" alt="Hybrid Hard/Soft Night Guard">
            </div>
            <div class="col-md-8 align-self-center">
                <h3>Hybrid Hard/Soft Night Guard</h3>
                <p>The hybrid night guard is crafted with two layers. The first layer will lay directly atop your patient's teeth and provide a smooth occlusal surface to prevent any damage during grinding or clenching. The second layer is made of a soft, polyurethane material that increases the comfort for patients at night. This option provides increased comfort and is a good option for patients who do not experience severe bruxism. </p>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection