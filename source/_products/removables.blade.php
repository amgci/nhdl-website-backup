@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Conventional Removables',
    'meta_description' => 'NHDL offer expertly crafted full arch restorations which will allow your patients to continue their daily lives with a stable and long lasting smile.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Conventional Removables'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>At New Horizons Dental Laboratory, we understand that often your edentulous patients would rather not have to deal with a denture. Our focus is always on patient satisfaction, therefore we provide several options for full arch solutions. </p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row product-listing">
        <div class="col-md-4">
            <div class="product-thumb">
                <a href="/products/removable/economy-dentures">
                    <img class="product-thumb" src="/img/Avadent_Denture.png" alt="Economy/Interim Dentures">
                    <h4>Economy/Interim Dentures</h4>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="product-thumb">
                <a href="/products/removable/immediate-dentures">
                    <img class="product-thumb" src="/img/Complete-Denture.png" alt="Immediate Dentures">
                    <h4>Immediate Dentures</h4>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="product-thumb">
                <a href="/products/removable/complete-dentures">
                    <img class="product-thumb" src="/img/FullDenture.png" alt="Full/Complete Dentures">
                    <h4>Full/Complete Dentures</h4>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="product-thumb">
                <a href="/products/removable/acrylic-partials">
                    <img class="product-thumb" src="/img/TCS-Flex-Partial.png" alt="Acrylic Partials">
                    <h4>Acrylic Partials</h4>
                </a>
            </div>
        </div>
        <div class="col-md-4">
            <div class="product-thumb">
                <a href="/products/removable/framework-partials">
                    <img class="product-thumb" src="/img/Partial-With-Cast-Full-View.png" alt="Framework Partials">
                    <h4>Framework Partials</h4>
                </a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection