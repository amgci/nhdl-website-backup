@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Acrylic Partials',
    'meta_description' => 'The acrylic partials from New Horizons Dental Laboratory are carefully crafted to ensure superior comfort and esthetics.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Conventional Removables'])
<section class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/TCS-Flex-Partial.png" alt="Acrylic Partials">
            </div>
            <div class="col-md-8 align-self-center">
                <h2>Acrylic Partials</h2>
                <p>The acrylic partials from New Horizons Dental Laboratory are carefully crafted to ensure superior comfort and aesthetics. Fabricated out of high-quality denture acrylic, we skillfully design the partial to seamlessly blend with your patients existing dentition. Your patients will be highly satisfied with the long-lasting durability and aesthetics of their acrylic partial. </p>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection