@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Full/Complete Dentures',
    'meta_description' => 'Our full denture prosthetics are carefully fabricated by our technicians in our Colorado-based dental lab.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Conventional Removables'])
<section class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/FullDenture.png" alt="Full/Complete Dentures">
            </div>
            <div class="col-md-8 align-self-center">
                <h2>Full/Complete Dentures</h2>
                <p>Our complete denture prosthetics are carefully fabricated by our technicians in our Colorado-based dental lab. We have been crafting complete dentures for over 30 years, which means your patient's smile is safe in our capable hands. We utilize the latest materials, processes, and equipment to ensure lifelike function and aesthetics, so your patients can live their lives without worry or discomfort. </p>
                <p>The use of the latest technology also ensures highly precise fit by our:</p>
                <ul>
                    <li>Detailed Step by Step Process</li>
                    <li>Detailed Fittings</li>
                </ul>
                <p>This allows your patient to laugh, smile, and talk without worry of changes to their voice or accidental slips or loss of suction. You can ensure patient satisfaction with our complete dentures, which not only restore oral function, but also renew a patient's confidence in their smile. </p>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection