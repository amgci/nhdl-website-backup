@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Economy/Interim Dentures',
    'meta_description' => 'Our economy or interim denture is the perfect solution for patients requiring a cost-effective or temporary removable prosthetic.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Conventional Removables'])
<section class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/Avadent_Denture.png" alt="Economy/Interim Dentures">
            </div>
            <div class="col-md-8 align-self-center">
                <h2>Economy/Interim Dentures</h2>
                <p>Our economy or interim denture is the perfect solution for patients requiring a cost-effective or temporary removable prosthetic. Fabricated out of high-quality materials, this denture still provides the necessary aesthetics and durability your patient is looking for, while also remaining affordable. </p>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection