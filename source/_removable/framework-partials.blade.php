@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Framework Partials',
    'meta_description' => 'The framework partials from New Horizons Dental Laboratory are a strong and durable solution for partially edentulous patients.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Conventional Removables'])
<section class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/Partial-With-Cast-Full-View.png" alt="Framework Partials">
            </div>
            <div class="col-md-8 align-self-center">
                <h2>Framework Partials</h2>
                <p>Expertly fabricated, each framework will perfectly fit alongside your patients existing dentition. The cast framework is crafted out of high-quality materials that will not warp, cause discomfort to your patients, or increase the likelihood of further tooth loss.</p>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection