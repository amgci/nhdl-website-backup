@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Immediate Dentures',
    'meta_description' => 'The immediate dentures from NHDL are an esthetic solution for patients requiring a removable prosthetic right away.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Conventional Removables'])
<section class="container">
        <div class="row">
            <div class="col-md-4">
                <img class="product-thumb" src="/img/Complete-Denture.png" alt="Immediate Dentures">
            </div>
            <div class="col-md-8 align-self-center">
                <h2>Immediate Dentures</h2>
                <p>The immediate dentures from New Horizons Dental Laboratory provide an aesthetic solution for patients requiring a removable prosthetic right away. These dentures are fabricated with great care at our laboratory. All our removable restorations are fabricated out of high-quality materials. The New Horizons Dental Lab team utilizes the highest laboratory techniques. </p>
                <p>By utilizing cutting edge technology and best in class materials we can provide your patients with:</p>
                <ul>
                    <li>Aesthetics</li>
                    <li>Function</li>
                    <li>Quick Turnaround</li>
                </ul>
            </div>
        </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection