@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Case Studies',
    'meta_description' => 'New Horizons Dental Laboratory stands shoulder-to-shoulder with dentists in pursuit of a common goal: dental health and patient satisfaction.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Case Studies'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>New Horizons Dental Laboratory believes we should walk our talk, which is why we provide information case studies, so you can see just how our team has benefited practices in the past. </p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12">
            <h2 class="mb-4 text-center">Day of Surgery Chairside Assistance</h2>
        </div>
        <div class="col-12 col-md-4">
            <img src="/img/Case-Study-1.png" class="mb-3" style="border: solid 2px #363d42;" alt="Case Study"><br>
            <img src="/img/Case-Study-2.png" style="border: solid 2px #363d42;" alt="Case Study">
        </div>
        <div class="col-12 col-md-8">
            <p>Full-arch restorations/All-on-4® cases can be complicated and takes months to complete. New Horizons' skilled technicians are there with you every step of the way from planning to completion. We ensure this lengthy process goes as smoothly as possible by providing individualized attention and partnering with you by assisting chairside on the day of surgery. </p>
            <ul>
                <li><span style="font-weight-bold">Patient:</span> Late 40s, Female</li>
                <li><span style="font-weight-bold">Doctor/Practice:</span> Dr. Kari Amick and Dr. Benjamin Howard</li>
                <li><span style="font-weight-bold">The Case:</span> Patient required an upper All-on-4® surgery.</li> 
                <li><span style="font-weight-bold">How NHDL Helped:</span> New Horizons Dental Laboratory was chairside during the patient's surgery to convert the previously made denture into a provisional denture (interim prosthesis) for the patient to use during healing. After the patient is fully healed, NHDL will continue to work with Dr. Amick and Dr. Howard until the final prosthesis is completed.</li>
                <li><span style="font-weight-bold">Why This Case Matters:</span> We provided personalized service that empowered Dr. Amick to complete this kind of work, with our assistance. The case exemplifies the partnership that New Horizons Dental Laboratory creates with the doctor.</li>
            </ul>
        </div>
    </div>
</section>
@endsection

@section('scripts')

@endsection