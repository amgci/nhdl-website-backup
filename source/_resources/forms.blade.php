@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Forms',
    'meta_description' => 'Below you will find our library of free resources that will assist you in your partnership with our laboratory.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Forms'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>Below you will find our library of free resources that will assist you in your partnership with our laboratory. We invite you to utilize these resources whenever you need to and contact our team if you need further assistance. </p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/Traditional-Removable-Rx-Form.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-rx"></div></div>
                    <h3>Removable Rx Form</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/Full-Arch-Restoration-All-on-4-Rx-Form.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-rx"></div></div>
                    <h3>All-on-4® Rx Form</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/Flowchart-Hybrid-Denture-Bar-Restoration.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-pdf"></div></div>
                    <h3>Flowchart: Final Bar Restoration</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/Flowchart-Bar-Retained-OverDenture.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-pdf"></div></div>
                    <h3>Flowchart: Bar Retained</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/Flowchart-Locator-Retained-OverDenture.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-pdf"></div></div>
                    <h3>Flowchart: Locator Retained</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/NHDL-Credit-Card-Authorization-Form.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-pdf"></div></div>
                    <h3>Credit Card Authorization</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/NHDL-e-payment-ACH-Authorization.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-pdf"></div></div>
                    <h3>E-Payment ACH Authorization</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/Torque-Spec-Sheet.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-specs"></div></div>
                    <h3>Implant Torque Specs</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <div class="resource-btn">
                <a href="/img/ADA-Code-List.pdf" target="_blank">
                    <div class="res-bkgd-wrap"><div class="res-bkgd res-code"></div></div>
                    <h3>ADA Code List</h3>
                    <hr>
                    <div class="btn">Download</div>
                </a>
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')

@endsection