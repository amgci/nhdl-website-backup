@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Case Scheduler',
    'meta_description' => 'Fill out all necessary information below and discover when your case will be returned to your practice.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Case Scheduler'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>Fill out all necessary information below and discover when your case will be returned to your practice.</p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="form-container">
                <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                <form id="caseCal-Form" method="post" name="CalForm" action="">
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="date-calendarform" placeholder="Select a Ship Date" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <select class="form-control" name="product" id="product-calendarform" placeholder="Select a Product">
                            <option value="" style="background-color: #f6f6f6;" disabled selected>Select a Product</option>
                            <option value="4">Baseplate/Wax Rim</option>
                            <option value="4">Custom Tray</option>
                            <option value="4">Flipper (1-3 teeth)</option>
                            <option value="9">Immediate Denture</option>
                            <option value="8">Partial-Processed Acrylic (> 3)</option>
                            <option value="5">Process and Finish </option>
                            <option value="1">Reline/Repair</option>
                            <option value="8">Splint</option>
                            <option value="8">Set-Up and Wax Try In (including Screw Retained Full Arch Resorations)</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Get Turnaround Time</button>
                </form>
                <div class="loader">Loading...</div>
            </div>  
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#caseCal-Form').submit(function(e) {
            e.preventDefault();
            $('#caseCal-Form ~ .loader').show();
            $('.case-calendar').remove();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/case-calendar-new',
                data: {
                    date: $('#date-calendarform').eq(0).val(),
                    product: $('#product-calendarform').eq(0).val(),
                    shiptime: "2",
                    processingtime: "0",
                    holidays: '["2018-11-21", "2018-11-23", "2018-12-26", "2018-12-31"]'
                },
                success: function(data) {
                    $('#caseCal-Form ~ .loader').hide();
                    $('#caseCal-Form').parent('.form-container').after(atob(data.calendar));
                    $('#delivery-disclaimer').show();
                }, 
                error: function() {

                }
            });
        });

        $( "#date-calendarform" ).datepicker();
    });
</script>
@endsection