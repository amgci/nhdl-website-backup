@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Local Pick-Up',
    'meta_description' => 'Our delivery drivers pickup and delivery cases from practices within our local area.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Local Pick-Up'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>Our delivery drivers pickup and deliver cases from practices within our local area (15 mile radius from our lab). We also offer courier pick up for practices further away for a small fee. Please provide us with your practice's address and one of our drivers will be on their way right away.</p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12">
            <div class="form-container">
                <form id="local-pickup-form" action="">
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                            </div>
                        </div>
                        <div class="col-lg-2 px-lg-0">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="form-label-group mb-3">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="ready" id="ready-today" value="Today" />
                            <label class="form-check-label" for="ready-today">
                                Ready today
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="ready" id="ready-tomorrow" value="Tomorrow" />
                            <label class="form-check-label" for="ready-tomorrow">
                                Ready tomorrow
                            </label>
                        </div>
                    </div>
                    <input type="hidden" id="public_id" value="71eaa30aa80a4879ab0da2a6dac07fa9" />
                <div class="g-recaptcha" data-sitekey="6LcFe5gUAAAAAC2ridE-9fv03DtO5uC3M80nWE80"></div>
                    <button type="submit" class="btn btn-primary">Request Pickup</button>
                </form>
                <div class="loader">Loading...</div>
            </div>     
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#local-pickup-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#contactForm .alert').remove();
            $('#local-pickup-form ~ .loader').show();
            $.ajax({
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/contact/71eaa30aa80a4879ab0da2a6dac07fa9/b886b4d4deb147f7a56b8306889a42f1',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-pickupform').eq(0).val(),
                    name: $('#doctor-name-pickupform').eq(0).val(),
                    address: $('#address-pickupform').val(),
                    city: $('#city-pickupform').eq(0).val(),
                    state: $('#state-pickupform').eq(0).val(),
                    zip: $('#zip-pickupform').eq(0).val(),
                    phone: $('#phone-pickupform').eq(0).val(),
                    ready: $('input[name="ready"]').eq(0).val()
                },
                success: function(data) {
                    $('#local-pickup-form ~ .loader').hide();
                    $('#btn-delivery-next').show();
                    $('#local-pickup-form').after('<p>Thanks for your request! We\'ll process your pickup shortly.</p>');
                }, 
                error: function() {

                }
            });
        });
    });
</script>
@endsection