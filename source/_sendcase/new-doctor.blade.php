@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'New Doctor',
    'meta_description' => 'At New Horizons Dental Laboratory, we believe that it should be easy to get your case to our team no matter what. We have streamlined our case submission process.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'New Doctor'])
<section class="container">
    <div class="row">
            <div class="col-12 text-center">
                <h2>Get Started Now</h2>
                <h3>Everything You Need for Easy and Accurate Case Submission</h3>
                <p>To get started download your Rx form, then select shipping options and schedule the case. This process is designed for efficiency and should only take few minutes.</p>
            </div>
        </div>
</section>
<section id="new-doctor">
    <div class="container">
        <div class="row mb-5">
            <div class="col-12 col-lg-8 offset-lg-2 d-none d-md-flex justify-content-between">
                <div class="step-num active" id="step-num1">
                    <div>
                        <span>1</span>
                    </div>
                </div>
                <div class="step-num" id="step-num2">
                    <div>
                        <span>2</span>
                    </div>
                </div>
                <div class="step-num" id="step-num3">
                    <div>
                        <span>3</span>
                    </div>
                </div>
<!--
                <div class="step-num" id="step-num4">
                    <div>
                        <span>4</span>
                    </div>
                </div>
-->
            </div>
        </div>
        <div class="row" id="steps">
            <div id="step-1" class="col-12 case-step text-center">
            <h3>Download Rx Form</h3>
                <p>We happily provide printable Rx forms for all your cases.</p>
                <a href="/img/Traditional-Removable-Rx-Form.pdf" class="btn" target="_blank">Download Removable Rx Form</a><br>
                <a href="/img/Full-Arch-Restoration-All-on-4-Rx-Form.pdf" class="btn" target="_blank">Download All-On-4&reg; Rx Form</a><br>
                <div id="move-to-2" class="btn">Continue <i class="fas fa-arrow-right"></i></div>
<!--
                <h3 class="mb-4">Select a Traditional or a Digital Case</h3>
                <div class="row">
                    <div class="col-12 col-md-6">
                        <h4 class="mb-3">Send a Traditional Case</h4>
                        <p>Your traditional case will be on its way to the lab in no time.  </p>
                        <div id="move-to-2" class="btn">Get Started</div>
                    </div>
                    <div class="col-12 col-md-6">
                        <h4 class="mb-3">Send a Digital Case</h4>
                        <p>We accept scans from the major intraoral scanners. </p>
                          <a href="/send-case/digital-case/" class="btn">Get Started</a> 
                    </div>
                </div>
-->
            </div>
            <div id="step-2" class="col-12 case-step d-none">
            <h3>Choose Your Delivery Method</h3>
                <p>Our delivery drivers will gladly retrieve your case from your practice. Please provide us with your zip code to see if you qualify. If you are not a local doctor, we invite you to choose your preferred method of shipping to ensure your case gets to our laboratory quickly and safely. We will gladly ship your case back for free as long as the cost is beneath $15.</p>
                <div id="formContainer" class="zipField">
                    <form method="post" name="Form" id="FORM" action="">
                        <input name="zipcode" type="var" placeholder="Enter Zip Code Here" id="zipcode_Field"><br>
                        <input class="btn mt-3" value="Submit" type="submit" style="width: 250px">
                    </form>
                    <div class="placeholder"></div>   
                </div>
                <div class="form-container">
                    <form id="local-pickup-form" style="display: none;" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-2 px-lg-0">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-label-group mb-3 text-left">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="ready" id="ready-today" value="Today" />
                                <label class="form-check-label" for="ready-today">
                                    Ready today
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="ready" id="ready-tomorrow" value="Tomorrow" />
                                <label class="form-check-label" for="ready-tomorrow">
                                    Ready tomorrow
                                </label>
                            </div>
                        </div>
                        <div class="text-left">
                            <input type="hidden" id="public_id" value="71eaa30aa80a4879ab0da2a6dac07fa9" />
                            <div class="g-recaptcha" data-sitekey="6LcFe5gUAAAAAC2ridE-9fv03DtO5uC3M80nWE80"></div>
                            <button type="submit" class="btn btn-primary">Request Pickup</button>
                        </div>
                    </form>
                    <div class="loader">Loading...</div>
                </div>
                <div id="move-to-3" class="btn">Continue <i class="fas fa-arrow-right"></i></div>
<!--
                <h3>Download Rx Form</h3>
                <p>We happily provide printable Rx forms for all your cases.</p>
                <a href="/img/Traditional-Removable-Rx-Form.pdf" class="btn" target="_blank">Download Removable Rx Form</a><br>
                <a href="/img/Full-Arch-Restoration-All-on-4-Rx-Form.pdf" class="btn" target="_blank">Download All-On-4&reg; Rx Form</a><br>
                <div id="move-to-3" class="btn">Continue <i class="fas fa-arrow-right"></i></div>
-->
            </div>
            <div id="step-3" class="col-12 case-step d-none">
                <h3>You're Finished!</h3>
                <p>We hope you found our case submission process quick and simple. We have placed all the resources you have used today in the menu above below Send a Case. We invite you to print Rx forms, shipping labels, and schedule local pickup anytime you send us a case. </p>
                <h4>Optional: Schedule Your Case</h4>
                <p>We want to ensure your case arrives at your practice when it is scheduled, which is why we have provided a convenient case scheduling calendar. Provide us with your case information and we will let you know when you can expect your restoration back at your practice.</p>
                <a href="/send-case/case-scheduler/" class="btn">Schedule Your Case</a>
<!--
                <h3>Choose Your Delivery Method</h3>
                <p>Our delivery drivers will gladly retrieve your case from your practice. Please provide us with your zip code to see if you qualify. If you are not a local doctor, we invite you to choose your preferred method of shipping to ensure your case gets to our laboratory quickly and safely. We will gladly ship your case back for free as long as the cost is beneath $15.</p>
                <div id="formContainer" class="zipField">
                    <form method="post" name="Form" id="FORM" action="">
                        <input name="zipcode" type="var" placeholder="Enter Zip Code Here" id="zipcode_Field"><br>
                        <input class="btn mt-3" value="Submit" type="submit" style="width: 250px">
                    </form>
                    <div class="placeholder"></div>   
                </div>
                <div class="form-container">
                    <form id="local-pickup-form" style="display: none;" action="">
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="doctor-name-pickupform" placeholder="Doctor's Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="practice-name-pickupform" placeholder="Practice Name" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="phone-pickupform" placeholder="Phone Number" required="required" type="text" />
                        </div>
                        <div class="form-label-group mb-3" style="margin: auto;">
                            <input class="form-control" id="address-pickupform" placeholder="Address" required="required" type="text" />
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="city-pickupform" placeholder="City" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-2 px-lg-0">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="state-pickupform" placeholder="State" required="required" type="text" />
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-label-group mb-3" style="margin: auto;">
                                    <input class="form-control" id="zip-pickupform" placeholder="Zipcode" required="required" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="form-label-group mb-3 text-left">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="ready" id="ready-today" value="Today" />
                                <label class="form-check-label" for="ready-today">
                                    Ready today
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="ready" id="ready-tomorrow" value="Tomorrow" />
                                <label class="form-check-label" for="ready-tomorrow">
                                    Ready tomorrow
                                </label>
                            </div>
                        </div>
                        <div class="text-left">
                            <input type="hidden" id="public_id" value="71eaa30aa80a4879ab0da2a6dac07fa9" />
                            <div class="g-recaptcha" data-sitekey="6LcFe5gUAAAAAC2ridE-9fv03DtO5uC3M80nWE80"></div>
                            <button type="submit" class="btn btn-primary">Request Pickup</button>
                        </div>
                    </form>
                    <div class="loader">Loading...</div>
                </div>
                <div id="move-to-4" class="btn">Continue <i class="fas fa-arrow-right"></i></div>
-->
            </div>
            <div id="step-4" class="col-12 case-step d-none">
<!--
                <h3>You're Finished!</h3>
                <p>We hope you found our case submission process quick and simple. We have placed all the resources you have used today in the menu above below Send a Case. We invite you to print Rx forms, shipping labels, and schedule local pickup anytime you send us a case. </p>
                <h4>Optional: Schedule Your Case</h4>
                <p>We want to ensure your case arrives at your practice when it is scheduled, which is why we have provided a convenient case scheduling calendar. Provide us with your case information and we will let you know when you can expect your restoration back at your practice.</p>
                <a href="/send-case/case-scheduler/" class="btn">Schedule Your Case</a>
-->
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        
        $('#step-num1').click(function(){
            moveTo1();
        })
        $('#step-num2').click(function(){
            moveTo2();
        })
        $('#move-to-2').click(function(){
            moveTo2();
        });
        $('#step-num2').click(function(){
            moveTo2();
        })
        $('#move-to-3').click(function(){
            moveTo3();
        });
        $('#step-num3').click(function(){
            moveTo3();
        })
        $('#move-to-4').click(function(){
            moveTo4();
        });
        $('#step-num4').click(function(){
            moveTo4();
        })
        
        function moveTo1() {
            $('#step-num1').addClass('active');
            $('#step-num2').removeClass('active');
            $('#step-num3').removeClass('active');
            $('#step-num4').removeClass('active');
            $('#step-1').removeClass('d-none');
            $('#step-2').addClass('d-none');
            $('#step-3').addClass('d-none');
            $('#step-4').addClass('d-none');
        }
        function moveTo2() {
            $('#step-num1').removeClass('active');
            $('#step-num2').addClass('active');
            $('#step-num3').removeClass('active');
            $('#step-num4').removeClass('active');
            $('#step-1').addClass('d-none');
            $('#step-2').removeClass('d-none');
            $('#step-3').addClass('d-none');
            $('#step-4').addClass('d-none');
        }
        function moveTo3() {
            $('#step-num1').removeClass('active');
            $('#step-num2').removeClass('active');
            $('#step-num3').addClass('active');
            $('#step-num4').removeClass('active');
            $('#step-1').addClass('d-none');
            $('#step-2').addClass('d-none');
            $('#step-3').removeClass('d-none');
            $('#step-4').addClass('d-none');
        }
        function moveTo4() {
            $('#step-num1').removeClass('active');
            $('#step-num2').removeClass('active');
            $('#step-num3').removeClass('active');
            $('#step-num4').addClass('active');
            $('#step-1').addClass('d-none');
            $('#step-2').addClass('d-none');
            $('#step-3').addClass('d-none');
            $('#step-4').removeClass('d-none');
        }
        
        //Zip Code Checker
        var zipCodes= ['80020','80038','80031','80234','80021','80023','80027','80005','80260','80614','80003','80241','80035','80030','80036','80233','80026','80007','80004','80221','80001','80006','80303','80229','80002','80602','80024','80305','80037','80516','80033','80640','80212','80034','80301','80211','80025','80309','80216','80514','80306','80307','80308','80310','80314','80214','80266','80202','80215','80205','80294','80265','80293','80204','80290','80264','80243','80244','80248','80250','80251','80256','80257','80259','80261','80263','80271','80273','80274','80281','80291','80299','80201','80217','80544','80402','80419','80203','80207','80218','80225','80304','80226','80206','80601','80238','80530','80262','80219','80520','80220','80239','80232','80209','80223','80022','80228','80040','80603','80041','80401','80246','80230','80502','80010','80501','80504','80047','80471','80045','80208','80210','80227','80302','80042','80224','80247','80236','80222','80403','80235','80110','80503','80533','80621','80150','80151','80155','80012','80453','80231'];

        $('#print-label-btn').hide();
        $('#local-pickup-btn').hide();

        function validateForm_Zip(zipcode) {
            if (zipcode==null || zipcode=="" || zipcode.length != 5) {
                window.alert("Please Enter a Valid Zipcode");
                return false;
            } else {
                return true;
            }
        }
        function isInArray(value, array) {
            return array.includes(value);
        }
        $('#FORM').submit(function(e){
            var zipcode=document.forms["Form"]["zipcode"].value;
            e.preventDefault();

            if( validateForm_Zip(zipcode) == false ){
                return;
            }
            if( isInArray(zipcode, zipCodes) == false ){
                $('.placeholder').empty()
                $('.placeholder').append('<div class="row"><div class="col-sm-12"><p style="margin: 15px 0px;">Your practice appears to be outside of our local pickup zone. We invite you to choose your preferred method of shipping to get your case to our lab. We will ship it back for free, as long as the cost is $15 or under.</div></div>')
                $('#local-pickup-form').hide();
                $('#local-pickup-btn').hide();
//                $('#print-label-btn').show();
                $('#move-to-4').show();
                $('#shipping-label-form').show();
            } else {
                $('.placeholder').empty()
                $('.placeholder').append('<div class="row"><div class="col-sm-12"><p style="margin: 15px 0px;">Hello, Neighbor! Your case qualifies for free pickup and delivery. We invite you to schedule a local pickup by providing us with your information.</div></div>')
                $('#local-pickup-form').show();
//                $('#print-label-btn').hide();
                $('#local-pickup-btn').show();
                $('#move-to-4').show();
            } 
        });
        
        //Local Pickup Form
        $('#local-pickup-form').submit(function(e) {
            e.preventDefault();
            $(this).hide();
            $('#contactForm .alert').remove();
            $('#local-pickup-form ~ .loader').show();
            $.ajax({
                
                
                method: 'POST',
                url: 'https://sheikah.amgservers.com/api/contact/71eaa30aa80a4879ab0da2a6dac07fa9/b886b4d4deb147f7a56b8306889a42f1',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-pickupform').eq(0).val(),
                    name: $('#doctor-name-pickupform').eq(0).val(),
                    address: $('#address-pickupform').val(),
                    city: $('#city-pickupform').eq(0).val(),
                    state: $('#state-pickupform').eq(0).val(),
                    zip: $('#zip-pickupform').eq(0).val(),
                    phone: $('#phone-pickupform').eq(0).val(),
                    ready: $('input[name="ready"]').eq(0).val()
                },
                success: function(data) {
                    $('#local-pickup-form ~ .loader').hide();
                    $('#btn-delivery-next').show();
                    $('#local-pickup-form').after('<p>Thanks for your request! We\'ll process your pickup shortly.</p>');
                }, 
                error: function() {

                }
            });
        });
        
    });
</script>
@endsection