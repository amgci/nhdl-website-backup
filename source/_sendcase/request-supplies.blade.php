@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Request Supplies',
    'meta_description' => 'NHDL has everything you need to prescribe our restorations. Submit a request below for case boxes, Rx forms, and our fee schedule.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Request Supplies'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>It is our responsibility to ensure you have all the necessary supplies on hand. Here you can request additional case boxes, or if you need additional supplies please contact us today for pricing and availability.</p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12">
        <!-- SharpSpring Form for NHDL - Request Supplies  -->
        <script type="text/javascript">
            var ss_form = {'account': 'MzawMDE3NzY1AAA', 'formID': 'SzZPMTRKTjTRNUi2NNA1MUw01rWwNDDWNbAwtEwzNDFNNjZOAwA'};
            ss_form.width = '100%';
            ss_form.height = '1000';
            ss_form.domain = 'app-3QNJWPZVN4.marketingautomation.services';
            // ss_form.hidden = {'field_id': 'value'}; // Modify this for sending hidden variables, or overriding values
            // ss_form.target_id = 'target'; // Optional parameter: forms will be placed inside the element with the specified id
            // ss_form.polling = true; // Optional parameter: set to true ONLY if your page loads dynamically and the id needs to be polled continually.
        </script>
        <script type="text/javascript" src="https://koi-3QNJWPZVN4.marketingautomation.services/client/form.js?ver=2.0.1"></script>
<!--
            <div class="form-container">
                <form id="request-supplies-form" action="">
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="doctor-name-suppliesform" placeholder="Doctor's Name" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="practice-name-suppliesform" placeholder="Practice Name" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="phone-suppliesform" placeholder="Phone Number" required="required" type="text" />
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <input class="form-control" id="address-suppliesform" placeholder="Address" required="required" type="text" />
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="city-suppliesform" placeholder="City" required="required" type="text" />
                            </div>
                        </div>
                        <div class="col-lg-2 px-lg-0">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="state-suppliesform" placeholder="State" required="required" type="text" />
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-label-group mb-3" style="margin: auto;">
                                <input class="form-control" id="zip-suppliesform" placeholder="Zipcode" required="required" type="text" />
                            </div>
                        </div>
                    </div>
                    <div class="form-label-group mb-3" style="margin: auto;">
                        <textarea id="message-suppliesform" class="form-control" placeholder="What supplies do you need?"></textarea>
                    </div>
                    <input type="hidden" id="public_id" value="71eaa30aa80a4879ab0da2a6dac07fa9" />
                    <div class="g-recaptcha" data-sitekey="6LcFe5gUAAAAAC2ridE-9fv03DtO5uC3M80nWE80"></div>
                    <button type="submit" class="btn btn-primary">Request Supplies</button>
                </form>
                <div class="loader">Loading...</div>
            </div> 
-->
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
//    $(document).ready(function() {
//        var formPending = false;
//        $('#request-supplies-form').submit(function(event) {
//            event.preventDefault();
//            if (formPending)
//                return;
//            formPending = true;
//            $(this).hide();
//            $('#request-supplies-form .alert').remove();
//            $('.loader').show();
//            $.ajax({
//                url: 'https://sheikah.amgservers.com/api/contact/71eaa30aa80a4879ab0da2a6dac07fa9/4bfa5dd4c1c24862a47f759bd521a38a',
//                method: 'post',
//                data: {
//                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
//                    id: $('#public_id').eq(0).val(),
//                    practice: $('#practice-name-suppliesform').eq(0).val(),
//                    name: $('#doctor-name-suppliesform').eq(0).val(),
//                    phone: $('#phone-suppliesform').eq(0).val(),
//                    address: $('#address-suppliesform').eq(0).val(),
//                    city: $('#city-suppliesform').eq(0).val(),
//                    state: $('#state-suppliesform').eq(0).val(),
//                    zip: $('#zip-suppliesform').eq(0).val(),
//                    supplies: $('#message-suppliesform').eq(0).val()
//                },
//                success: function(data) {
//                    $('.loader').hide();
//                    $('#request-supplies-form').after('<p>Thanks for your request! We\'ll get in touch with you as soon as possible!</p>');
//                },
//                error: function(data, status, err) {
//                    $('.loader').hide();
//                    $('#request-supplies-form').show();
//                    formPending = false;
//                    $('#request-supplies-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
//                }
//            });
//        });
//    });
</script>
@endsection