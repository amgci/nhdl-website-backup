@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Turnaround Times',
    'meta_description' => 'Below is a listing of all our turnaround times. Please be aware that this is an approximation of the number of days needed in the lab until delivery.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Turnaround Times'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>Below is a listing of all our turnaround times. Please be aware that this is an approximation of the number of days needed in the lab until delivery. These turnaround times do not include the time it takes a case to be shipped to and from our lab. </p>
                <p><a href="/img/NHDL-Days-to-Delivery.pdf" class="btn" target="_blank">Download Turnaround Times</a></p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-12">
            <ul>
                <li><span style="font-weight: 600;">Baseplate/Wax Rim</span> – 4 days</li>
                <li><span style="font-weight: 600;">Custom Tray</span> – 4 days</li>
                <li><span style="font-weight: 600;">Flipper (1-3 teeth)</span> – 4 days</li>
                <li><span style="font-weight: 600;">Immediate Denture</span> – 9 days</li>
                <li><span style="font-weight: 600;">Partial-Metal Framework</span> – Based on # of days in frame lab plusthe # of days needed atNew Horizons Laboratory thereafter</li>
                <li><span style="font-weight: 600;">Partial-Processed Acrylic (> 3)</span> – 8 days</li>
                <li><span style="font-weight: 600;">Process and Finish</span> – 5-6 days</li>
                <li><span style="font-weight: 600;">Reline (courier and distance dependent)</span> – All attempts are made for same day or within 36 hours</li>
                <li><span style="font-weight: 600;">Repair</span> – Dependent on case. All Attempts are made for same day or within 36 hours (please note: NHDL does not employ a night shift)</li>
                <li><span style="font-weight: 600;">Splint</span> – 8 days </li>
                <li><span style="font-weight: 600;">Set-Up and Wax Try In (including Screw Retained Full Arch Resorations)</span> – 8 days</li>
                <li><span style="font-weight: 600;">2nd Wax Try In</span> – Each Case is Assessed Individually</li>
                <li>In Cases of Emergency-Please <a href="/contact-us/">Contact Us</a></li>
            </ul>  
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection