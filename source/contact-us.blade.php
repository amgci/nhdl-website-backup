@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Contact Us',
    'meta_description' => 'If you have any questions, comments, or concerns, please let NHDL know so we can provide you with the best service and restorations possible.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Contact Us'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2>Get in Touch with Us</h2>
                <p>As your partner, New Horizons Dental Lab gets better through your feedback. If you have any questions, comments, or concerns, please let us know so we can provide you with the best service and restorations possible.</p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-md-5">
            <h5>New Horizons Dental Laboratory</h5>
            <address> 7270 W. 118th Place UNIT D <br>
                Broomfield, Colorado 80020</address>
            <p><span class="font-weight-bold">Phone:</span> 303.469.3362 <br>
            <span class="font-weight-bold">Email:</span> newhorizonslab@gmail.com <br>
            <span class="font-weight-bold">Fax:</span> 303.469.0002</p>
        </div>
        <div class="col-md-7">
            <h5>Contact Form</h5>
            <!-- SharpSpring Form for NHDL - Contact Us  -->
            <script type="text/javascript">
                var ss_form = {'account': 'MzawMDE3NzY1AAA', 'formID': 'S0xMTUlJskjVTTJOStY1MTMw0k1MMU3RNTI1NU5LMjNNTE1OBQA'};
                ss_form.width = '100%';
                ss_form.height = '1000';
                ss_form.domain = 'app-3QNJWPZVN4.marketingautomation.services';
                // ss_form.hidden = {'field_id': 'value'}; // Modify this for sending hidden variables, or overriding values
                // ss_form.target_id = 'target'; // Optional parameter: forms will be placed inside the element with the specified id
                // ss_form.polling = true; // Optional parameter: set to true ONLY if your page loads dynamically and the id needs to be polled continually.
            </script>
            <script type="text/javascript" src="https://koi-3QNJWPZVN4.marketingautomation.services/client/form.js?ver=2.0.1"></script>
            <!--
            <form id="contact-form" action="">
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="name-contactform" class="form-control" placeholder="Your Name" required="required" type="text" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="practice-name-contactform" class="form-control" placeholder="Practice Name" required="required" type="text" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="phone-contactform" class="form-control" placeholder="Phone Number" required="required" type="tel" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <textarea id="message-contactform" class="form-control" placeholder="What can we do for you?"></textarea>
                </div>
                <input type="hidden" id="public_id" value="71eaa30aa80a4879ab0da2a6dac07fa9" />
                <div class="g-recaptcha" data-sitekey="6LcFe5gUAAAAAC2ridE-9fv03DtO5uC3M80nWE80"></div>
                <button class="btn btn-primary mt-3" type="submit">Send Now</button>
            </form>
            <div class="loader">Loading...</div>
            -->
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
//    $(document).ready(function() {
//        var formPending = false;
//        $('#contact-form').submit(function(event) {
//            event.preventDefault();
//            if (formPending)
//                return;
//            formPending = true;
//            $(this).hide();
//            $('#contactForm .alert').remove();
//            $('.loader').show();
//            $.ajax({
//                url: 'https://sheikah.amgservers.com/api/contact/71eaa30aa80a4879ab0da2a6dac07fa9/d3a0c627a73546368e0d90ca747d59a3',
//                method: 'post',
//                data: {
//                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
//                    id: $('#public_id').eq(0).val(),
//                    practice: $('#practice-name-contactform').eq(0).val(),
//                    name: $('#name-contactform').eq(0).val(),
//                    phone: $('#phone-contactform').eq(0).val(),
//                    email: $('#email-contactform').eq(0).val(),
//                    message: $('#message-contactform').eq(0).val()
//                },
//                success: function(data) {
//                    $('.loader').hide();
//                    $('#contact-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
//                },
//                error: function(data, status, err) {
//                    $('.loader').hide();
//                    $('#contact-form').show();
//                    formPending = false;
//                    $('#contact-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
//                }
//            });
//        });
//    });
</script>
@endsection