@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Your Laboratory Partner. Every Step of the Way.',
    'meta_description' => 'Located in Broomfield, Colorado, New Horizons Dental Lab is a full arch and removables-focused dental laboratory.'
    ])
@endsection

@section('body')
<section id="home-banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Your Laboratory Partner. <br> Every Step of the Way.</h1>
                <h2>Your practice is committed to dental health, so is our lab.</h2>
            </div>
        </div>
    </div>
</section>
<section id="home-cta-icons">
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-center">
                <div class="cta-icon">
                    <img src="/img/Full-Arch-Restorations.svg" alt="Full-Arch Restorations Icon">
                </div>
                <div class="cta-txt">
                    <h2>Full-Arch <br> Restorations</h2>
                    <p>We deliver consistent and high-quality full arch restorations. We are committed to patient satisfaction and your efficient practice. We offer fixed and removable solutions for all your conventional and full arch Implant needs; the All-on-4® solution as well as the Trefoil™ from Nobel Biocare.</p>
                </div>
                <div class="cta-btn text-center">
                    <a href="/products/full-arch-restorations/" class="btn">Learn More</a>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="cta-icon">
                    <img src="/img/Conventional-Removables.svg" alt="Conventional Removables Icon">
                </div>
                <div class="cta-txt">
                    <h2>Conventional <br> Removables</h2>
                    <p>Whether your patients need full or partial dentures, our lab ensures that your patients have access to a wide variety of options that will best suit their needs for function, comfort, and aesthetics.</p>
                </div>
                <div class="cta-btn text-center">
                    <a href="/products/removables/" class="btn">Learn More</a>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="cta-icon">
                    <img src="/img/Additional_Services.svg" alt="Additional Services Icon">
                </div>
                <div class="cta-txt">
                    <h2>Additional <br> Services</h2>
                    <p>New Horizons Dental Laboratory is here to support your commitment to your practice and patients. We offer traditional services, such as dentures, partial dentures, and night guards. We also offer reline and repair services for all removable prosthetics, as well as in-depth collaborative opportunities, like chair-side assistance. </p>
                </div>
                <div class="cta-btn text-center">
                    <a href="/products/additional-services/" class="btn">Learn More</a>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="home-intro">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h2>Improving Outcomes for Your Patients and Your Practice</h2>
                <h3>End-to-end accountability is our commitment to you.</h3>
                <p>Here at New Horizons Dental Laboratory, we are not only experts in fabricating high-quality restorations; we are also industry leaders dedicated to partnering with and assisting dental practices throughout the nation. Whether you are focusing on advanced All-On-4 restorations or traditional methods, trust our team to guide you through every step of the restorative process. Depend on our team for: case planning, scanning and diagnostic services, chair-side assistance, and even model pouring classes.</p>
                <a href="/about-us/our-story/" class="btn">Learn More</a>
            </div>
            <div class="col-md-5 align-self-center">
                <img src="/img/8249-Home-CTA-Champa.png" alt="Home Image Champa">
            </div>
        </div>
    </div>
</section>
<section id="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Our Partners Tell Our Story</h2>
                <h3>Praise from our clients.</h3>
            </div>
        </div>
        <div class="row mb-2">
            <div class="col-md-4">
                <img src="/img/Testimonial-2-Thumb.png" alt="Home Testimonial Image 2">
                <p>"Complex cases are solved: they give me reliable results, I don't have to re-do work, they get it right the first time. Speed is good, patients aren't forced to wait a long time."</p>
                <h5>-Dr. Andrew Bock</h5>
            </div>
            <div class="col-md-4">
                <img src="/img/Testimonial-1-Thumb.png" alt="Home Testimonial Image 1">
                <p>"Their dentures are always the best fitting and looking. The night guards are great too. Minimal adjustments needed."</p>
                <h5>-Dr. Larry Tilliss</h5>
            </div>
            <div class="col-md-4">
                <img src="/img/Testimonial-3-Thumb.png" alt="Home Testimonial Image 3">
                <p>"NHDL's accuracy and knowledge create well-designed and well-fitting appliances"</p>
                <h5>-Dr. Kari Amick</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center">
               <a href="/about-us/testimonials/" class="btn">Read More</a> 
            </div>
            <div class="col-12 text-center">
               <a href="/leave-testimonial" class="btn">Leave a Testimonial</a> 
            </div>
        </div>
    </div>
</section>
<section id="get-started">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="/img/8249-Champa-and-Team.png" alt="Home Champa and Team">
            </div>
            <div class="col-md-7 align-self-center">
                <h2>Removing Stress from <br> Your Day-to-Day Workload</h2>
                <h3>Send Your First Case Today</h3>
                <p>Our case submission process is designed to take the stress out of your day-to-day workload. Your team will be able to easily submit new cases that get to our lab quickly. Our website contains all the forms and information that you need. Still have questions? We are standing by to answer them.</p>
                <a href="/send-case/new-doctor/" class="btn">Get Started</a>
            </div>
        </div>
    </div>
</section>
<section id="home-seo">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Devoted to Successful Partnerships</h2>
                <h3>Your Colorado-Based Dental Laboratory Partner</h3>
                <p>New Horizons Dental Laboratory is located in Broomfield, CO. We are a full arch and removables-focused dental laboratory that believes improved dental health deserves to be life-changing. Our expert technicians craft dental restorations that rejuvenate confidence and deliver big smiles.</p>
                <p>We use the best materials and processes available to ensure the restoration you receive will provide you with predictable results to ensure patient satisfaction. Combined with our end-to-end support, your practice can benefit from the efficiency and reliability of the New Horizons Dental Laboratory team. We invite you to partner with the leading Colorado-based full-arch and removable laboratory that will help you reduce chair time, increase overall satisfaction, and eliminate stress no matter the complexity of the case.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-3 offset-md-3">
               <a href="/about-us/our-process/" class="btn">Learn More</a> 
            </div>
            <div class="col-sm-12 col-md-3">
               <a href="/contact-us/" class="btn">Get in Touch</a> 
            </div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection