@extends('_layouts.master')

@section('meta')
@include('_partials.meta', [
    'meta_title' => 'Leave Testimonial',
    'meta_description' => 'If you have any questions, comments, or concerns, please let NHDL know so we can provide you with the best service and restorations possible.'
    ])
@endsection

@section('body')
@include('_partials.page-header', ['page_title' => 'Leave Testimonial'])
<section class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p>The NHDL team is incredibly grateful you wish to leave a testimonial of how our partnership has led to successful outcomes. Feel free to write as much or as little as you would like. </p>
                <p class="font-italic">Please note: testimonials may be edited solely to correct grammar or spelling.</p>
            </div>
        </div>
</section>
<section class="container">
    <div class="row">
        <div class="col-12">
            <h5>Testimonial Submission</h5>
            <form id="testimonial-form" action="">
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="name-contactform" class="form-control" placeholder="Your Name" required="required" type="text" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="practice-name-contactform" class="form-control" placeholder="Practice Name" required="required" type="text" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="phone-contactform" class="form-control" placeholder="Phone Number" required="required" type="tel" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <input id="email-contactform" class="form-control" placeholder="Email Address" required="required" type="email" />
                </div>
                <div class="form-label-group mb-3" style="margin: auto;">
                    <textarea id="message-contactform" class="form-control" placeholder="Your Testimonial"></textarea>
                </div>
                <input type="hidden" id="public_id" value="71eaa30aa80a4879ab0da2a6dac07fa9" />
                <div class="g-recaptcha" data-sitekey="6LcFe5gUAAAAAC2ridE-9fv03DtO5uC3M80nWE80"></div>
                <button class="btn btn-primary mt-3" type="submit">Send Now</button>
            </form>
            <div class="loader">Loading...</div>
        </div>
    </div>
</section>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var formPending = false;
        $('#testimonial-form').submit(function(event) {
            event.preventDefault();
            if (formPending)
                return;
            formPending = true;
            $(this).hide();
            $('#contactForm .alert').remove();
            $('.loader').show();
            $.ajax({
                url: 'https://sheikah.amgservers.com/api/contact/71eaa30aa80a4879ab0da2a6dac07fa9/3e2bd345ebd64d6898161d38ea982f34',
                method: 'post',
                data: {
                    'g-recaptcha-response': $('#g-recaptcha-response').eq(0).val(),
                    id: $('#public_id').eq(0).val(),
                    practice: $('#practice-name-contactform').eq(0).val(),
                    name: $('#name-contactform').eq(0).val(),
                    phone: $('#phone-contactform').eq(0).val(),
                    email: $('#email-contactform').eq(0).val(),
                    message: $('#message-contactform').eq(0).val()
                },
                success: function(data) {
                    $('.loader').hide();
                    $('#testimonial-form').after('<p>Thank you for contacting us! We\'ll get in touch with you as soon as possible!</p>');
                },
                error: function(data, status, err) {
                    $('.loader').hide();
                    $('#testimonial-form').show();
                    formPending = false;
                    $('#testimonial-form button[type="submit"]').before('<div class="alert alert-danger" role="alert">Please fill out all of the fields</div>');
                }
            });
        });
    });
</script>
@endsection